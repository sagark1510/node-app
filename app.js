const express = require('express');
const path = require('path');
const exphps = require('express-handlebars');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');


const app = express();

require('./config/passport')(passport);

// Db config
const db = require('./config/database');

// mpa global promise - get rid of warning
mongoose.Promise = global.Promise;
// Connect to mongoose 
mongoose.connect(db.mongoUri)
.then(() => {
    console.log('MongoDB connectted..');
})
.catch(() => {
    console.log('Mongo connection error...')
});

// Handlebars middleware
app.engine('handlebars', exphps({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');


// Body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Static folder
app.use(express.static(path.join(__dirname, 'public')));

// method override middleware
app.use(methodOverride('_method'));

// session middleware
app.use(session({
    secret: 'asdasdfdsfsfds',
    resave: false,
    saveUninitialized: true,
}));

// Passport middleware
app.use(passport.initialize());
app.use(passport.session())

// flash middleware
app.use(flash());

// Global variables
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
})

// Index route
app.get('/', (req, res) => {
    const title = 'Welcome';
    res.render('index', {title: title});
});

app.get('/about', (req, res) => {
    res.render('about')
});

// Load routes
const ideas = require('./routes/ideas');
const users = require('./routes/users');

// Use routes
app.use('/ideas', ideas);
app.use('/users', users);

const port = process.env.PORT || 5000;

app.listen(port, () => {
    console.log(`Server started on port - ${port}`);
});