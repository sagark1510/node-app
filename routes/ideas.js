const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const {ensureAuthenticated} = require('../helpers/auth');

// Load idea model
require('../models/Idea');
const Idea = mongoose.model('ideas');

router.get('/add', ensureAuthenticated, ensureAuthenticated, (req, res) => {
    res.render('ideas/add');
});

router.post('/', ensureAuthenticated, (req, res) => {
    let errors = [];
    if (!req.body.title) {
        errors.push({text: 'Please add a title'});
    }
    if (!req.body.details) {
        errors.push({text: 'Please add a some details'});
    }

    if (errors.length) {
        res.render('ideas/add', {
            errors: errors,
            title: req.body.title,
            details: req.body.details
        });
    } else {
        const newUser = {
            title: req.body.title,
            details: req.body.details,
            user: req.user.id,
        }
        new Idea(newUser)
        .save()
        .then((idea) => {
            req.flash('success_msg', 'Idea has been added succesfully.');
            res.redirect('/ideas');
        })
    }
});

router.get('/', ensureAuthenticated, (req, res) => {
    Idea.find({user: req.user.id})
    .sort({date: 'desc'})
    .then(ideas => {
        res.render('ideas/index', {ideas: ideas});
    })
});

router.get('/edit/:id', ensureAuthenticated, (req, res) => {
    Idea.findOne({
        _id: req.params.id,
    })
    .then(idea => {
        if (idea.user != req.user.id) {
            req.flash('error_msg', 'Not authorize');
            res.redirect('/ideas');
        } else {
            res.render('ideas/edit', {idea: idea});
        }
    })
    .catch(error => {
        req.flash('success_msg', 'Idea has been updated succesfully.');
        res.send(error);
    })
});

router.put('/:id', ensureAuthenticated, (req, res) => {
    Idea.findOne({_id: req.params.id})
    .then((idea) => {
        idea.title = req.body.title;
        idea.details = req.body.details;
        idea.save()
        .then(() => {
            res.redirect('/ideas');
        })
    });
})

router.delete('/:id', ensureAuthenticated, (req, res) => {
    // Idea.findOne({_id: req.params.id})
    // .then((idea) => {
    //     idea.remove()
    //     .then(() => {
    //         res.redirect('/ideas');
    //     })
    // });

    Idea.remove({_id: req.params.id})
    .then(() => {
        req.flash('success_msg', 'Idea has been deleted succesfully.');
        res.redirect('/ideas');
    })
})

module.exports = router;